<?php

namespace Infotechnohelp\ModelManager;

use App\ModelManager\Books;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use Symfony\Component\Yaml\Yaml;

/**
 * Class ModelManager
 * @package Infotechnohelp\ModelManager
 */
class ModelManager
{
    /**
     * @var \Cake\ORM\Table
     */
    protected $table;

    /**
     * ModelManager constructor.
     */
    public function __construct()
    {
        $this->table = TableRegistry::getTableLocator()->get($this->tableAlias);
    }

    /**
     * @param $data
     * @return \Cake\Datasource\EntityInterface
     */
    public function insertRawData($data)
    {
        $Entity = $this->table->newEntity($data);

        return $this->table->saveOrFail($Entity);
    }

    /**
     * @return array
     */
    private function defaultFindOptions()
    {
        return [
            'include' => 'all', // 'first' || 'last'
            'array' => false,
            'containAll' => false,

            'findType' => null,
            'contain' => [],
            'matching' => [],
            'notMatching' => [],
            'order' => [],
            'select' => [],
            'limit' => null,
            'extra' => null,

            'distinct' => true,
        ];
    }

    /**
     * @param array $options
     * @return array
     */
    private function prepareOptions(array $options = [])
    {
        $result = $this->defaultFindOptions();

        if (file_exists(CONFIG . 'model-manager.yml')) {
            $config = Yaml::parse(file_get_contents(CONFIG . 'model-manager.yml'));

            if (array_key_exists('find', $config)) {
                foreach ($config['find'] as $key => $value) {
                    $result[$key] = $value;
                }
            }
        }

        foreach ($options as $key => $value) {
            $result[$key] = $value;
        }

        return $result;
    }

    /**
     * @param array $conditions
     * @param string $tableAlias
     * @return array
     */
    private function prepareConditions(array $conditions, string $tableAlias)
    {
        $result = [];

        $tableAliasArray = explode(".", $tableAlias);

        $tableAlias = array_pop($tableAliasArray);

        foreach ($conditions as $key => $value) {

            if ($key === 'OR') {

                $subResult = [];

                foreach ($value as $subConditions) {

                    $subResult2 = [];

                    foreach ($subConditions as $subKey => $subValue) {
                        $subResult2["$tableAlias.$subKey"] = $subValue;
                    }

                    $subResult[] = $subResult2;
                }

                $result[$key] = $subResult;

                continue;
            }

            if (is_numeric($key)) {
                $result[] = "$tableAlias.$value";
                continue;
            }

            $result["$tableAlias.$key"] = $value;
        }

        return $result;
    }

    public function all(array $options = [], string $findBy = null, $needle = null)
    {
        $newOptions = array_merge($options, ['include' => 'all']);

        return $this->find($newOptions, $findBy, $needle);
    }

    public function first(array $options = [], string $findBy = null, $needle = null)
    {
        $newOptions = array_merge($options, ['include' => 'first']);

        return $this->find($newOptions, $findBy, $needle);
    }

    public function last(array $options = [], string $findBy = null, $needle = null)
    {
        $newOptions = array_merge($options, ['include' => 'last']);

        return $this->find($newOptions, $findBy, $needle);
    }


    /**
     * @param array $options
     * @param string|null $findBy
     * @param null $needle
     * @return array|\Cake\Datasource\EntityInterface|\Cake\Datasource\ResultSetInterface|mixed|null
     */
    public function find(array $options = [], string $findBy = null, $needle = null)
    {
        $Query = $this->table->find();

        if (!empty($_['find'])) {
            $Query = $this->table->find($_['find']);
        }

        if ($findBy !== null) {
            $methodTitle = "findBy$findBy";
            $Query = $this->table->{$methodTitle}($needle);
        }

        $_ = $this->prepareOptions($options);

        switch ($_['include']) {
            case 'last':
                $Query->orderDesc("{$this->tableAlias}.id");
            case 'first':
                $Query->limit(1);
        }


        if ($_['containAll']) {

            foreach ($this->containAllAliases as $alias) {
                // Get first alias
                $alias = explode(".", $alias)[0];

                $this->table->associations()->get($alias)->setJoinType('LEFT');
            }

            $Query->contain($this->containAllAliases);
        }

        if (!empty($_['contain'])) {
            foreach ($_['contain'] as $config) {

                list($alias, $conditions, $callback, $joinType) = array_pad($config, 4, null);

                if (empty($conditions) && !empty($joinType)) {
                    $this->table->associations()->get($alias)->setJoinType($joinType);
                    $Query->contain($alias, $callback);

                    continue;
                }

                if (!empty($conditions) && !empty($joinType)) {
                    $Query->contain([
                        $alias => [
                            'conditions' => $this->prepareConditions($conditions, $alias),
                            'joinType' => $joinType,
                        ],
                    ]);

                    continue;
                }

                if (!empty($conditions)) {

                    $Query->contain([
                        $alias => [
                            'conditions' => $this->prepareConditions($conditions, $alias),
                        ],
                    ]);

                    continue;
                }


                $Query->contain($alias);
            }
        }

        if (!empty($_['matching'])) {
            foreach ($_['matching'] as $config) {

                list($alias, $conditions, $callback) = array_pad($config, 3, null);

                if (!empty($conditions)) {
                    $Query->matching($alias, function (Query $Query) use ($conditions, $alias) {

                        return $Query->where($this->prepareConditions($conditions, $alias));
                    });

                    continue;
                }


                $Query->matching($alias, $callback);
            }
        }

        if (!empty($_['notMatching'])) {
            foreach ($_['notMatching'] as $config) {

                list($alias, $conditions, $callback) = array_pad($config, 3, null);


                if (!empty($conditions)) {
                    $Query->notMatching($alias, function (Query $Query) use ($conditions, $alias) {
                        return $Query->where($this->prepareConditions($conditions, $alias));
                    });

                    continue;
                }


                $Query->notMatching($alias, $callback);
            }
        }

        if (!empty($_['where'])) {
            $Query->where($_['where']);
        }


        if (!empty($_['order'])) {
            $Query->order($_['order']);
        }

        if (!empty($_['limit'])) {
            $Query->limit($_['limit']);
        }

        if (!empty($_['select'])) {
            $Query->select($_['select']);
        }


        if (!empty($_['extra'])) {
            $Query = $_['extra']($Query);
        }

        if ($_['distinct']) {
            $Query->distinct(["{$this->tableAlias}.id"]);
        }

        switch ($_['include']) {
            case 'first':
            case 'last':
                $result = $Query->first();
                break;
            default:
                $result = $Query->all();
        }


        if ($_['array']) {
            $result = json_decode(json_encode($result), true);
        }


        return $result;

    }

    /**
     * @param int $id
     * @param array $data
     * @return array
     */
    public function updateEntity(int $id, array $data)
    {
        $Entity = $this->find([
            'include' => 'first', 'containAll' => true,
        ], 'Id', $id);

        $Entity = $this->table->patchEntity($Entity, $data);

        return [$this->table->saveOrFail($Entity), $Entity->getErrors()];
    }

    // @todo Avoid duplications
    public function removeAssociatedIds($entityId, $field, $associatedIds)
    {
        $Entity = $this->find([
            'include' => 'first', 'containAll' => true,
        ], 'Id', $entityId);

        $existingAssociatedIds = [];

        foreach ($Entity->get($field) as $associatedEntity) {
            $existingAssociatedIds[] = $associatedEntity->get('id');
        }

        $Table = TableRegistry::getTableLocator()->get($this->tableAlias);

        $Entity = $Table->patchEntity($Entity, [

            $field => [
                '_ids' => array_diff($existingAssociatedIds, $associatedIds),
            ],

        ]);

        return $Table->saveOrFail($Entity);
    }

    public function addAssociatedIds($entityId, $field, $associatedIds)
    {
        $Entity = $this->find([
            'include' => 'first', 'containAll' => true,
        ], 'Id', $entityId);

        $existingAssociatedIds = [];

        $entities = ($Entity->get($field) === null) ? [] : $Entity->get($field);

        foreach ($entities as $associatedEntity) {
            $existingAssociatedIds[] = $associatedEntity->get('id');
        }

        $Table = TableRegistry::getTableLocator()->get($this->tableAlias);

        $Entity = $Table->patchEntity($Entity, [

            $field => [
                '_ids' => array_merge($existingAssociatedIds, $associatedIds),
            ],

        ]);

        return $Table->saveOrFail($Entity);
    }


    /**
     * @param array $ids
     * @return bool|mixed
     * @throws \Exception
     */
    public function deleteByIds(array $ids)
    {
        return $this->transactional(function () use ($ids) {
            foreach ($ids as $id) {
                $this->table->deleteOrFail($this->table->get($id));
            }
        });
    }

    public function transactional($transactional)
    {
        return $this->table->getConnection()->transactional($transactional);
    }
}